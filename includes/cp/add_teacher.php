
<form id="form1" name="form1" method="post" action="includes/cp/add_teacher_function.php">
    
   	<div class="form-group row">
        <label for="Matricule" class="col-xs-12 col-md-2 identifie2"> Matricule </label>
        <input type="text"  id="Matricule" name="teacher_id" class="col-xs-12 col-md-10"/>
	
    </div>
	<div class="form-group row">
        <label for="Nom4" class="col-xs-12 col-md-2 identifie2">Nom</label>
        <input type="text" name="nom" id="Nom4" placeholder="Ex:Rabeh" required="required" class="col-xs-12 col-md-10"/>
	</div>
	<div class="form-group row">
		<label for="Prenom" class="col-xs-12 col-md-2 identifie2">Prenom</label>
		<input type="text" name="prenom" id="Prenom" placeholder="Ex:Amina" required="required"  class="col-xs-12 col-md-10"/>
	</div>
	<div class="form-group row">
		<label for="grade" class="col-xs-12 col-md-2">Grade</label>
		<select name="grade" id="grade" class="select col-xs-12 col-md-10">
			<option value=""></option>
			<?php
				$grades_query = mysqli_query($con, "SELECT id, nom FROM grade");
				while($grade_row = mysqli_fetch_assoc($grades_query)){
					echo'<option value="'.$grade_row['id'].'">'.$grade_row['nom'].'</option>';
				} 
			?>
		</select>
	</div>
	<div class="form-group row">
		<label for="Date de naissance" class="col-xs-12 col-md-2 identifie2">Date de naissance</label>
		<input type="date" name="date_de_naissance" id="Date de naissance" class="col-xs-12 col-md-10"/>
	</div>
	<div class="form-group row">
        <label for="adress_mail" class="col-xs-12 col-md-2 identifie2">Email</label>
        <input type="text" name="adresse_mail" id="adresse_mail" placeholder="Email" required="required"  class="col-xs-12 col-md-10"/>
	</div>
	<div class="form-group row">
        <label for="password" class="col-xs-12 col-md-2 identifie2">Mot de pass</label>
        <input type="password" name="password" id="password" placeholder="Mot de pass.." required="required"  class="col-xs-12 col-md-10"/>
	</div>
	<div class="form-group row">
        <label for="password_conf" class="col-xs-12 col-md-2 identifie2">Confirmé le mot de pass</label>
        <input type="password" name="password_conf" id="password_conf" placeholder="rentrer le mot de pass.." required="required"  class="col-xs-12 col-md-10"/>
	</div>
	<div class="form-group row">
		<label for="Lieu de naissance" class="col-xs-12 col-md-2 identifie2">Lieu de naissance </label>
		<input type="text" name="lieu_de_naissance" id="Lieu de naissance" placeholder="Ex:Relizane" class="col-xs-12 col-md-10"required="required" />
	</div>
    <div class="form-group row">
		<label for="" class="col-xs-12 col-md-2 identifie2">Telephone </label>
		<input type="text" name="telephone" id="Telephone" placeholder="Ex:0781514599" class="col-xs-12 col-md-10"required="required" />
	</div>
		
	<div class="form-group row">
        	<label for="institut" class="col-xs-12 col-md-2">Institut</label>
			<select name="institut" id="institut" class="select col-xs-12 col-md-10">
				<option value=""></option>
				<?php
					$niveaus_query = mysqli_query($con,"SELECT id, nom FROM institut ORDER BY id ASC");
					while($niveau_row = mysqli_fetch_assoc($niveaus_query)){
						echo"<option value=".$niveau_row['id'].">".$niveau_row['nom']."</option>";
					}
				?>
			</select>
		</div>
		<div class="form-group row">
        	<label for="domaine" class="col-xs-12 col-md-2">Domaine</label>
			<select name="domaine" id="domaine" class="select col-xs-12 col-md-10">
				<option value=""></option>
				<?php
					$niveaus_query = mysqli_query($con,"SELECT id, nom FROM domaine ORDER BY id ASC");
					while($niveau_row = mysqli_fetch_assoc($niveaus_query)){
						echo"<option value=".$niveau_row['id'].">".$niveau_row['nom']."</option>";
					}
				?>
			</select>
		</div>
		<div class="form-group row">
        	<label for="filiere" class="col-xs-12 col-md-2">Filière</label>
			<select name="filiere" id="filiere" class="select col-xs-12 col-md-10">
				<option value="0"></option>
				<?php
					$niveaus_query = mysqli_query($con,"SELECT id, nom FROM filiere ORDER BY id ASC");
					while($niveau_row = mysqli_fetch_assoc($niveaus_query)){
						echo"<option value=".$niveau_row['id'].">".$niveau_row['nom']."</option>";
					}
				?>
			</select>
		</div>
		<div class="form-group row">
        	<label for="specialite" class="col-xs-12 col-md-2">Specialité</label>
			<select name="specialite" id="specialite" class="select col-xs-12 col-md-10">
				<option value="0"></option>
				<?php
					$niveaus_query = mysqli_query($con,"SELECT id, nom FROM specialite ORDER BY id ASC");
					while($niveau_row = mysqli_fetch_assoc($niveaus_query)){
						echo"<option value=".$niveau_row['id'].">".$niveau_row['nom']."</option>";
					}
				?>
			</select>
		</div>
	<div class="form-group row">
        <input type="reset" value=" Actualiser" class="envoie" />
		<input  type="submit" class="envoie" value=" Ajouter " />
		</div>
    </form>
    