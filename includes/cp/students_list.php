<?php
$search_query = "";
$page_query = "";
if(!empty($_GET["mat"])){
    if(preg_match("/^[0-9]{12}+$/", $_GET["mat"])){
        $search_query .= " WHERE matricule = '".$_GET["mat"]."' ";
        $page_query .= "&mat=".$_GET["mat"];
    }
}

if(!empty($_GET["nom"])){
    if(preg_match("/^[a-zA-Z ]+$/", $_GET["nom"])){
        strlen($search_query) > 0 ? $search_query .= " AND " : $search_query .= " WHERE ";
        $search_query .= "nom LIKE '%".$_GET["nom"]."%' ";
        $page_query .= "&nom=".$_GET["nom"];
    }
}
if(!empty($_GET["prenom"])){
    if(preg_match("/^[a-zA-Z ]+$/", $_GET["prenom"])){
        strlen($search_query) > 0 ? $search_query .= " AND " : $search_query .= " WHERE ";
        $search_query .= "prenom LIKE '%".$_GET["prenom"]."%' ";
        $page_query .= "&prenom=".$_GET["prenom"];
    }
}
if(!empty($_GET["institut"])){
    if(preg_match("/^[1-9][0-9]{0,1}+$/", $_GET["institut"])){
        strlen($search_query) > 0 ? $search_query .= " AND " : $search_query .= " WHERE ";
        $search_query .= "institut = '".$_GET["institut"]."' ";
        $page_query .= "&institut=".$_GET["institut"];
    }
}
if(!empty($_GET["domaine"])){
    if(preg_match("/^[1-9][0-9]{0,1}+$/", $_GET["domaine"])){
        strlen($search_query) > 0 ? $search_query .= " AND " : $search_query .= " WHERE ";
        $search_query .= "domaine = '".$_GET["domaine"]."' ";
        $page_query .= "&domaine=".$_GET["domaine"];
    }
}
if(!empty($_GET["filiere"])){
    if(preg_match("/^[1-9][0-9]{0,1}+$/", $_GET["filiere"])){
        strlen($search_query) > 0 ? $search_query .= " AND " : $search_query .= " WHERE ";
        $search_query .= "filiere = '".$_GET["filiere"]."' ";
        $page_query .= "&filiere=".$_GET["filiere"];
    }
}
if(!empty($_GET["specialite"])){
    if(preg_match("/^[1-9][0-9]{0,1}+$/", $_GET["specialite"])){
        strlen($search_query) > 0 ? $search_query .= " AND " : $search_query .= " WHERE ";
        $search_query .= "specialite = '".$_GET["specialite"]."' ";
        $page_query .= "&specialite=".$_GET["specialite"];
    }
}
if(!empty($_GET["niveau"])){
    if(preg_match("/^[1-9][0-9]{0,1}+$/", $_GET["niveau"])){
        strlen($search_query) > 0 ? $search_query .= " AND " : $search_query .= " WHERE ";
        $search_query .= "niveau = '".$_GET["niveau"]."' ";
        $page_query .= "&niveau=".$_GET["niveau"];
    }
}

//echo' <h1>'.$search_query.'</h1> ';
?>
<style>
    .search_elements{
        width: 200px;
        height: 30px;
        margin: 5px;
    }
</style>
<div class="row" style="margin-bottom: 50px; background-color: #eee; border-radius: 10px; padding: 20px 50px;">
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get">
        <div class="col-xs-6 col-md-4 row">
            <div class="col-xs-12">
                <input type="text" class="search_elements" name="mat" id="mat" placeholder="Le matricule d'etudiant"/>
            </div>
            <div class="col-xs-12">
                <input type="text" class="search_elements" name="nom" id="nom" placeholder="Le nom d'etudiant"/>
            </div>
            <div class="col-xs-12">
                <input type="text" class="search_elements" name="prenom" id="prenom" placeholder="Le prénom d'etudiant"/>
            </div>
        </div>
        <div class="col-xs-6 col-md-4 row">
            <div class="col-xs-12">
                <select class="search_elements" style="float:right;"  name="niveau" id="niveau">
                    <option value="">--choisi un niveau--</option>
                    <?php
                    $filiere_query = mysqli_query($con, "SELECT id,nom FROM niveau");
                    while($filiere_row = mysqli_fetch_assoc($filiere_query)){
                        echo" <option value='".$filiere_row['id']."'>".$filiere_row['nom']."</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="col-xs-12">
                <select class="search_elements" style="float:right;"  name="institut" id="institut">
                    <option value="">--choisi un institut--</option>
                    <?php
                    $filiere_query = mysqli_query($con, "SELECT id,nom FROM institut");
                    while($filiere_row = mysqli_fetch_assoc($filiere_query)){
                        echo" <option value='".$filiere_row['id']."'>".$filiere_row['nom']."</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="col-xs-12">
                <select class="search_elements" style="float:right;"  name="domaine" id="domaine">
                    <option value="">--choisi un domaine--</option>
                    <?php
                    $filiere_query = mysqli_query($con, "SELECT id,nom FROM domaine");
                    while($filiere_row = mysqli_fetch_assoc($filiere_query)){
                        echo" <option value='".$filiere_row['id']."'>".$filiere_row['nom']."</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="col-xs-12">
                <select class="search_elements" style="float:right;"  name="filiere" id="filiere">
                    <option value="">--choisi un filiere--</option>
                    <?php
                    $filiere_query = mysqli_query($con, "SELECT id,nom FROM filiere");
                    while($filiere_row = mysqli_fetch_assoc($filiere_query)){
                        echo" <option value='".$filiere_row['id']."'>".$filiere_row['nom']."</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="col-xs-12">
                <select class="search_elements" style="float:right;"  name="specialite" id="specialite">
                    <option value="">--choisi un specialite--</option>
                    <?php
                    $filiere_query = mysqli_query($con, "SELECT id,nom FROM specialite");
                    while($filiere_row = mysqli_fetch_assoc($filiere_query)){
                        echo" <option value='".$filiere_row['id']."'>".$filiere_row['nom']."</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        
        <div class="col-xs-12 col-md-7">
            <button style="float:right;" class="btn btn-success btn-sm">Rechercher</button>
            <input type="hidden" name="section" value="<?php echo $_GET["section"]; ?>">
        </div>

    </form>
</div>
<div class="row">
    <div class="col-xs-12" style="margin-bottom:20px;">
        <button style="float:right;" class="btn btn-success btn-sm" id="addTeacher">
            <span class="glyphicon glyphicon-plus"></span>
            Ajouter un etudiant
        </button>
        <button style="float:right;margin-right: 10px;" class="btn btn-danger btn-sm" id="printListBtn">
            <span class="glyphicon glyphicon-print"></span>
            Imprimer la liste
        </button>
    </div>
</div>
<table id="toBePrinted" class="administratorsListTable table">
    <tr>
        <th>Matricule</th>
        <th>Nom</th>
        <th>Prenom</th>
        <th>Date de naissance</th>
        <th>lieu de naissance</th>
        <th>niveau</th>
        <th class="hideAtPrinting">Action</th>
    </tr>
    <?php
        $number = 10;
        $page = 1;
        if(!empty($_GET["page"]) && preg_match("/^[1-9][0-9]{0,4}$/",$_GET["page"])){
            $page = $_GET["page"];
        }
        $start = ($page-1)*$number;
        
        $administrators_info_query = mysqli_query($con, 
        "SELECT id,matricule, nom, prenom, date_naissance, lieu_naissance,niveau
        FROM etudiant 
        $search_query
        ORDER BY nom ASC 
        LIMIT 
        $start,$number");
        $count_administrators = mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(*) AS count_administrators FROM etudiant $search_query"));
        $count_administrators = $count_administrators["count_administrators"];

        //echo"<h1>".$count_students."<br>".mysqli_error($con)."<br>".$search_query."</h1>";
        $pages_count = ceil($count_administrators/$number);
        while ($administrator_info_row = mysqli_fetch_assoc($administrators_info_query )){
            $id = $administrator_info_row["id"];
            $matricule = $administrator_info_row["matricule"];
            $nom = $administrator_info_row["nom"];
            $prenom = $administrator_info_row["prenom"];
            $date_naissance = $administrator_info_row["date_naissance"];
            $lieu_naissance = $administrator_info_row["lieu_naissance"];
            $niveau = $administrator_info_row["niveau"];
            $niveau = mysqli_fetch_assoc(mysqli_query($con, "SELECT nom FROM niveau WHERE id='$niveau'"));
            $niveau = $niveau['nom'];
            echo"
            <tr>
                <td>$matricule</td>
                <td>$nom</td>
                <td>$prenom</td>
                <td>$date_naissance</td>
                <td>$lieu_naissance</td>
                <td>$niveau</td>
                <td class='hideAtPrinting' id='".$nom." ".$prenom."'>
                    <span name='remove_user_btn' id='".$id."' class='btn btn-danger btn-sm glyphicon glyphicon-remove'></span>
                    <span name='edit_user_btn'  id='".$id."' class='btn btn-info btn-sm glyphicon glyphicon-edit'></span></td>
            </tr>";
    }
    ?>
</table>
<nav aria-label="Page navigation example">
  <ul class="pagination">
    <?php
        if($page > 1){
            echo'<li class="page-item"><a class="page-link" href="#">&laquo;</a></li>';
        }
        
        for($i = 1; $i <= $pages_count; $i++){
            $page == $i ? $class = "active" : $class= "";
            echo'<li class="page-item '.$class.'"><a class="page-link" href="admincp.php?section=students_list&page='.$i.'">'.$i.'</a></li>';
       }
        if($page < $pages_count){
            echo'<li class="page-item"><a class="page-link" href="#">&raquo;</a></li>';
        }
    ?>
  </ul>
</nav>
<!-- Modal -->
<div class="modal fade" id="msgModal" tabindex="-1" role="dialog" aria-labelledby="msgModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="modalContent" class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" id="cancel" class="btn btn-secondary" data-dismiss="modal">Anuller</button>
        <button type="button" id="confirm" class="btn btn-primary">Confirmer</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="msgModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="editContentModal" class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" id="cancel" class="btn btn-secondary" data-dismiss="modal">Anuller</button>
        <button type="button" id="confirmEditBtn" class="btn btn-primary">Confirmer</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="msgModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="addContentModal" class="modal-body" style="padding: 25px;">      
   	<div class="form-group row">
       <label for="Matricule" class="col-xs-12 col-md-2 identifie2"> Matricule </label>
       <input type="text"  id="Matricule" name="add_teacher_id" class="col-xs-12 col-md-10"/>
   
   </div>
   <div class="form-group row">
       <label for="Nom4" class="col-xs-12 col-md-2 identifie2">Nom</label>
       <input type="text" name="add_nom" id="Nom4" placeholder="Ex:Rabeh" required="required" class="col-xs-12 col-md-10"/>
   </div>
   <div class="form-group row">
       <label for="Prenom" class="col-xs-12 col-md-2 identifie2">Prenom</label>
       <input type="text" name="add_prenom" id="Prenom" placeholder="Ex:Amina" required="required"  class="col-xs-12 col-md-10"/>
   </div>
   <div class="form-group row">
       <label for="niveau" class="col-xs-12 col-md-2">niveau</label>
       <select name="add_niveau" id="niveau" class="select col-xs-12 col-md-10">
           <option value=""></option>';
           <?php
               $niveaus_query = mysqli_query($con, "SELECT id, nom FROM niveau");
               while($niveau_row = mysqli_fetch_assoc($niveaus_query)){
                   echo'<option value="'.$niveau_row['id'].'">'.$niveau_row['nom'].'</option>';
               } 
           
               ?>
       </select>
   </div>
   <div class="form-group row">
       <label for="Date de naissance" class="col-xs-12 col-md-2 identifie2">Date de naissance</label>
       <input type="date" name="add_date_de_naissance" id="Date de naissance" class="col-xs-12 col-md-10"/>
   </div>
   <div class="form-group row">
       <label for="adress_mail" class="col-xs-12 col-md-2 identifie2">Email</label>
       <input type="text" name="add_adresse_mail" id="adresse_mail" placeholder="Email" required="required"  class="col-xs-12 col-md-10"/>
   </div>
   <div class="form-group row">
       <label for="password" class="col-xs-12 col-md-2 identifie2">Mot de pass</label>
       <input type="password" name="add_password" id="password" placeholder="Mot de pass.." required="required"  class="col-xs-12 col-md-10"/>
   </div>
   <div class="form-group row">
       <label for="password_conf" class="col-xs-12 col-md-2 identifie2">Confirmé le mot de pass</label>
       <input type="password" name="add_password_conf" id="password_conf" placeholder="rentrer le mot de pass.." required="required"  class="col-xs-12 col-md-10"/>
   </div>
   <div class="form-group row">
       <label for="Lieu de naissance" class="col-xs-12 col-md-2 identifie2">Lieu de naissance </label>
       <input type="text" name="add_lieu_de_naissance" id="Lieu de naissance" placeholder="Ex:Relizane" class="col-xs-12 col-md-10"required="required" />
   </div>
   <div class="form-group row">
       <label for="" class="col-xs-12 col-md-2 identifie2">Telephone </label>
       <input type="text" name="add_telephone" id="Telephone" placeholder="Ex:0781514599" class="col-xs-12 col-md-10"required="required" />
   </div>
       
        <div class="form-group row">
           <label for="institut" class="col-xs-12 col-md-2">Institut</label>
           <select name="add_institut" id="institut" class="select col-xs-12 col-md-10">
               <option value=""></option>';
               <?php
                   $niveaus_query = mysqli_query($con,"SELECT id, nom FROM institut ORDER BY id ASC");
                   while($niveau_row = mysqli_fetch_assoc($niveaus_query)){
                       echo"<option value=".$niveau_row['id'].">".$niveau_row['nom']."</option>";
                   }
               
                   ?>
           </select>
       </div>
       <div class="form-group row">
           <label for="domaine" class="col-xs-12 col-md-2">Domaine</label>
           <select name="add_domaine" id="domaine" class="select col-xs-12 col-md-10">
               <option value=""></option>';
               <?php
                   $niveaus_query = mysqli_query($con,"SELECT id, nom FROM domaine ORDER BY id ASC");
                   while($niveau_row = mysqli_fetch_assoc($niveaus_query)){
                       echo"<option value=".$niveau_row['id'].">".$niveau_row['nom']."</option>";
                   }
                   ?>
           </select>
       </div>
       <div class="form-group row">
           <label for="filiere" class="col-xs-12 col-md-2">filiere</label>
           <select name="add_filiere" id="filiere" class="select col-xs-12 col-md-10">
               <option value=""></option>';
               <?php
                   $niveaus_query = mysqli_query($con,"SELECT id, nom FROM filiere ORDER BY id ASC");
                   while($niveau_row = mysqli_fetch_assoc($niveaus_query)){
                       echo"<option value=".$niveau_row['id'].">".$niveau_row['nom']."</option>";
                   }
                   ?>
           </select>
       </div>
       <div class="form-group row">
           <label for="specialite" class="col-xs-12 col-md-2">specialite</label>
           <select name="add_specialite" id="specialite" class="select col-xs-12 col-md-10">
               <option value=""></option>';
               <?php
                   $niveaus_query = mysqli_query($con,"SELECT id, nom FROM specialite ORDER BY id ASC");
                   while($niveau_row = mysqli_fetch_assoc($niveaus_query)){
                       echo"<option value=".$niveau_row['id'].">".$niveau_row['nom']."</option>";
                   }
                   ?>
           </select>
       </div> 
      </div>
      <div class="modal-footer">
        <button type="button" id="cancel" class="btn btn-secondary" data-dismiss="modal">Anuller</button>
        <button type="button" id="confirmAddBtn" class="btn btn-primary">Confirmer</button>
      </div>
    </div>
  </div>
</div>
<script language="javascript">
$(document).ready(function(){
    let chosenId = "";
    $("[name='remove_user_btn']").on("click", function(){
        chosenId = $(this).attr("id");
        let username = $(this).parent().attr("id");
        $("#modalContent").html("êtes-vous sûr de vouloir supprimer "+username+" ?");
        $("#confirm").attr("name","delete");
        let confName = $("#confirm").attr("name");//alert(confName);
        $("#msgModal").modal("show");
    });
    $("[name='edit_user_btn']").on("click", function(){
        let id = $(this).attr("id");
        chosenId = id;
        let action = "edit";
        let username = $(this).parent().attr("id");
        $.post("includes/cp/student_action.php", {
            id : id,
            action: action
        }, function(data){
            $("#editContentModal").html(data);
            $("#editModal").modal("show");
        });
    });
    $("#confirm").on("click", function(){
        let id = chosenId;
        let action = $(this).attr("name");
        //alert(action);
        $("#msgModal").modal("hide");
        if(action == "delete"){
            //alert(0);
            $.post("includes/cp/student_action.php", {
                id : id,
                action: action
            }, function(data){
                $("#modalContent").html(data);
                $("#msgModal").modal("show");
                setInterval(function(){$("#msgModal").modal("hide");}, 3000);
            });
        }
    });
    
    $("#confirmEditBtn").on("click", function(){
            let id = chosenId;
            let action =  "real_edit";
            let username = $(this).parent().attr("id");
            let nom = $("[name ='edit_nom']").val();
            let prenom = $("[name ='edit_prenom']").val();
            let date_de_naissance = $("[name ='edit_date_de_naissance']").val();
            let adresse_mail = $("[name ='edit_adresse_mail']").val();
            let password = $("[name ='edit_password']").val();
            let lieu_de_naissance = $("[name ='edit_lieu_de_naissance']").val();
            let domaine = $("[name ='edit_domaine']").val();
            let specialite = $("[name ='edit_specialite']").val();
            let filiere = $("[name ='edit_filiere']").val();
            let institut = $("[name ='edit_institut']").val();  
            $.post("includes/cp/student_action.php", {
                id : id,
                action: action,
                nom: nom,
                prenom: prenom,
                date_de_naissance: date_de_naissance,
                adresse_mail: adresse_mail,
                password: password,
                lieu_de_naissance: lieu_de_naissance,
                domaine: domaine,
                institut: institut,
                specialite: specialite,
                filiere: filiere
            },function(data,success){
                $("#editContentModal").html(data);
                $("#editModal").modal("show");
                setInterval(function(){$("#editModal").modal("hide"); location.reload();}, 3000);
            });
    });
    $("#addTeacher").on("click", function(){
        $("#addModal").modal("show");
    });
    $("#confirmAddBtn").on("click", function(){
        let nom = $("[name ='add_nom']").val();
        let prenom = $("[name ='add_prenom']").val();
        let teacher_id = $("[name ='add_teacher_id']").val();
        let date_de_naissance = $("[name ='add_date_de_naissance']").val();
        let adresse_mail = $("[name ='add_adresse_mail']").val();
        let password = $("[name ='add_password']").val();
        let password_conf = $("[name ='add_password_conf']").val();
        let lieu_de_naissance = $("[name ='add_lieu_de_naissance']").val();
        let niveau = $("[name ='add_niveau']").val();
        let domaine = $("[name ='add_domaine']").val();
        let institut = $("[name ='add_institut']").val();
        let specialite = $("[name ='add_specialite']").val();
        let filiere = $("[name ='add_filiere']").val();
        let telephone = $("[name = 'add_telephone']").val();
    
        $.post("includes/cp/student_action.php",{
            id: teacher_id,
            action: "add_teacher",
            nom: nom,
            prenom: prenom,
            teacher_id: teacher_id,
            date_de_naissance: date_de_naissance,
            telephone: telephone,
            adresse_mail: adresse_mail,
            password: password,
            password_conf: password_conf,
            lieu_de_naissance: lieu_de_naissance,
            niveau: niveau,
            domaine: domaine,
            institut: institut,
            specialite: specialite,
            filiere: filiere
        },function(data,success){
            alert(data);
        });
    });
    $("#printListBtn").on("click", function(){
        $(".hideAtPrinting").css({"display":"none"});
        $("#toBePrinted").printThis({pageTitle: "LISTE DES ETUDIANTS"});
        setTimeout(function(){
            $(".hideAtPrinting").css({"display":"block"});
        }, 3000);
        
    });
    
});
</script>