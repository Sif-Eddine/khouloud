<?php 
require("../config.php");
/*echo $_POST["administrator_id"];
echo $_POST["Nom"];
echo $_POST["Prenom"];
echo $_POST["Telephone"];
echo $_POST["date_de_naissance"];
echo $_POST["lieu_de_naissance"];
echo $_POST["adresse_mail"];*/
$msg = "";
$error_counter = 0;
if(empty($_POST["adminstrator_id"])){exit("undefined administrator_id!");}
if(empty($_POST["nom"])){exit("undefined Nom!");}
if(empty($_POST["prenom"])){exit("undefined Prenom!");}
if(empty($_POST["telephone"])){exit("undefined Telephone!");}
if(empty($_POST["date_de_naissance"])){exit("undefined date_de_naissance!");}
if(empty($_POST["lieu_de_naissance"])){exit("undefined lieu_de_naissance!");}
if(empty($_POST["adresse_mail"])){exit("undefined adresse_mail!");}
if(empty($_POST["password"])){exit("undefined password!");}
if(empty($_POST["password_conf"])){exit("undefined password confirmation!");}
$mat= $_POST["adminstrator_id"];
$nom = $_POST["nom"];
$prenom = $_POST["prenom"];
$telephone = $_POST["telephone"];
$date_de_naissance = $_POST["date_de_naissance"];
$lieu_de_naissance = $_POST["lieu_de_naissance"];
$adresse_mail = $_POST["adresse_mail"];
$password = $_POST["password"];
$password_conf = $_POST["password_conf"];
$date_de_naissance_test = explode( "-", $date_de_naissance);
$year = $date_de_naissance_test[0];
$month = $date_de_naissance_test[1];
$day = $date_de_naissance_test[2];
if($year > (date("Y") - 16) OR $year < 1920 OR $month < 1 OR $month >12 OR $day< 1 or $day>31){ $msg = $msg."administrator birthdate is wrong!"; $error_counter++;}
if(!preg_match("/^[0-9]{12}$/", $mat)){$msg = $msg."administrator_id is wrong! <br>"; $error_counter ++;}
if(!preg_match("/^[0][0-9]{9}$/", $telephone)){$msg = $msg."Telephone is wrong! <br>"; $error_counter ++;}
if(!preg_match("/^[a-zA-Z ]+$/", $nom)){$msg = $msg."Nom is wrong! <br>"; $error_counter ++;}
if(!preg_match("/^[a-zA-Z ]+$/", $prenom)){$msg = $msg."Prenom is wrong! <br>"; $error_counter ++;}
if(!preg_match("/^[a-zA-Z ]+$/", $lieu_de_naissance)){$msg = $msg."Lieu de naissance is wrong! <br>"; $error_counter ++;}
if(!filter_var($adresse_mail,FILTER_VALIDATE_EMAIL)){$msg = $msg."adresse_mail is wrong! <br>"; $error_counter ++;}
if(strlen($password) < 8 OR strlen($password) > 16 OR $password != $password_conf){$msg = $msg."password is wrong! <br>"; $error_counter ++;}

if($error_counter == 0){
    $count_existing_student = mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(*) AS x FROM administrateur WHERE matricule='$mat' OR (nom='$nom' AND prenom='$prenom' AND date_naissance='$date_de_naissance')"));
    echo mysqli_error($con);
    if($count_existing_student['x'] > 0){exit("This user already exists!");}
    $crypted_pass = md5($password);
    if(mysqli_query($con,"INSERT INTO `administrateur` (`matricule`, `nom`, `prenom`, `date_naissance`, `lieu_naissance`, `telephone`,`email`,`password`,`time`) VALUES ('$mat', '$nom', '$prenom', '$date_de_naissance', '$lieu_de_naissance', '$telephone','$adresse_mail','$crypted_pass','$time')")){$msg = "User was successfully inserted!";}
    else{echo mysqli_error($con);}
}
echo $msg;
?>َ