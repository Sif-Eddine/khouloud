<?php 
require("../config.php");
/*echo $_POST["student_id"];
echo $_POST["nom"];
echo $_POST["prenom"];
echo $_POST["niveau"];
echo $_POST["date_de_naissance"];
echo $_POST["lieu_de_naissance"];
echo $_POST["adresse_mail"];*/
$msg = "";
$error_counter = 0;
if(empty($_POST["student_id"])){exit("undefined student_id!");}
if(empty($_POST["nom"])){exit("undefined nom!");}
if(empty($_POST["prenom"])){exit("undefined prenom!");}
if(empty($_POST["niveau"])){exit("undefined niveau!");}
if(empty($_POST["institut"])){exit("undefined institut!");}
if(empty($_POST["domaine"])){exit("undefined domaine!");}
if(empty($_POST["filiere"])){exit("undefined filiere!");}
if(empty($_POST["date_de_naissance"])){exit("undefined date_de_naissance!");}
if(empty($_POST["lieu_de_naissance"])){exit("undefined lieu_de_naissance!");}
if(empty($_POST["adresse_mail"])){exit("undefined adresse_mail!");}
if(empty($_POST["password"])){exit("undefined password!");}
if(empty($_POST["password_conf"])){exit("undefined password confirmation!");}
if(empty($_POST["specialite"])){exit("undefined specialite!");}

$mat = $_POST["student_id"];
$nom = $_POST["nom"];
$prenom = $_POST["prenom"];
$niveau = $_POST["niveau"];
$institut = $_POST["institut"];
$domaine = $_POST["domaine"];
$filiere = $_POST["filiere"];
$specialite = $_POST["specialite"];
$date_de_naissance = $_POST["date_de_naissance"];
$lieu_de_naissance = $_POST["lieu_de_naissance"];
$adresse_mail = $_POST["adresse_mail"];
$password = $_POST["password"];
$password_conf = $_POST["password_conf"];
$date_de_naissance_test = explode( "-", $date_de_naissance);
$year = $date_de_naissance_test[0];
$month = $date_de_naissance_test[1];
$day = $date_de_naissance_test[2];
if($year > (date("Y") - 16) OR $year < 1920 OR $month < 1 OR $month >12 OR $day< 1 or $day>31){ $msg = $msg."Student birthdate is wrong!"; $error_counter++;}
if(!preg_match("/^[0-9]{12}+$/", $mat)){$msg = $msg."student_id is wrong! <br>"; $error_counter ++;}
if(!preg_match("/^[a-zA-Z ]+$/", $nom)){$msg = $msg."nom is wrong! <br>"; $error_counter ++;}
if(!preg_match("/^[a-zA-Z ]+$/", $prenom)){$msg = $msg."prenom is wrong! <br>"; $error_counter ++;}
if(!preg_match("/^[1-9][0-9]{0,1}+$/", $niveau)){$msg = $msg."niveau is wrong! <br>"; $error_counter ++;}
if(!preg_match("/^[1-9][0-9]{0,1}+$/", $institut)){$msg = $msg."institut is wrong! <br>"; $error_counter ++;}
if(!preg_match("/^[1-9][0-9]{0,1}+$/", $domaine)){$msg = $msg."domaine is wrong! <br>"; $error_counter ++;}
if(!preg_match("/^[1-9][0-9]{0,1}+$/", $filiere)){$msg = $msg."filiere is wrong! <br>"; $error_counter ++;}
if(!preg_match("/^[1-9][0-9]{0,1}+$/", $specialite)){$msg = $msg."specialite is wrong! <br>"; $error_counter ++;}

if(!preg_match("/^[a-zA-Z ]+$/", $lieu_de_naissance)){$msg = $msg."Lieu de naissance is wrong! <br>"; $error_counter ++;}
if(!filter_var($adresse_mail,FILTER_VALIDATE_EMAIL)){$msg = $msg."adresse_mail is wrong! <br>"; $error_counter ++;}
if(strlen($password) < 8 OR strlen($password) > 16 OR $password != $password_conf){$msg = $msg."password is wrong! <br>"; $error_counter ++;}

if($error_counter == 0){
    $count_existing_student = mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(*) AS x FROM etudiant WHERE matricule='$mat' OR (nom='$nom' AND prenom='$prenom' AND date_naissance='$date_de_naissance')"));
    echo mysqli_error($con);
    if($count_existing_student['x'] > 0){exit("This user already exists!");}
    $crypted_pass = md5($password);
    if(mysqli_query($con,"INSERT INTO `etudiant` (`matricule`, `nom`, `prenom`, `date_naissance`, `lieu_naissance`, `niveau`, `institut`, `domaine`, `filiere`, `specialite`,`email`,`password`,`time`) VALUES ('$mat', '$nom', '$prenom', '$date_de_naissance', '$lieu_de_naissance', '$niveau', '$institut', '$domaine', '$filiere', '$specialite','$adresse_mail','$crypted_pass','$time')")){$msg = "User was successfully inserted!";}
    else{echo mysqli_error($con);}
}
echo $msg;
?>َ