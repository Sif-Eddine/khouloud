         
    <form id="form1" name="form1" method="post" action="includes/cp/add_student_function.php">
		<div class="form-group row">
        	<label for="student_id" class="col-xs-12 col-md-2 identifie2"> Numero_inscription </label>
        	<input type="text"  id="student_id" name="student_id" class="col-xs-12 col-md-10"/>
		</div>
		<div class="form-group row">
        	<label for="nom4" class="col-xs-12 col-md-2 identifie2">Nom</label>
        	<input type="text" name="nom" id="nom4" placeholder="Ex:Belhadj" required="required" class="col-xs-12 col-md-10"/>
		</div>
		<div class="form-group row">
			<label for="prenom" class="col-xs-12 col-md-2 identifie2">Prenom</label>
			<input type="text" name="prenom" id="prenom" placeholder="Ex:Amina" required="required"  class="col-xs-12 col-md-10"/>
		</div>
		<div class="form-group row">
			<label for="Date de naissance" class="col-xs-12 col-md-2 identifie2">Date de naissance</label>
			<input type="date" name="date_de_naissance" id="Date de naissance" class="col-xs-12 col-md-10"/>
		</div>
		<div class="form-group row">
        	<label for="adress_mail" class="col-xs-12 col-md-2 identifie2">Email</label>
        	<input type="text" name="adresse_mail" id="adresse_mail" placeholder="Email" required="required"  class="col-xs-12 col-md-10"/>
		</div>
		<div class="form-group row">
        	<label for="password" class="col-xs-12 col-md-2 identifie2">Mot de pass</label>
        	<input type="password" name="password" id="password" placeholder="Mot de pass.." required="required"  class="col-xs-12 col-md-10"/>
		</div>
		<div class="form-group row">
        	<label for="password_conf" class="col-xs-12 col-md-2 identifie2">Confirmé le mot de pass</label>
        	<input type="password" name="password_conf" id="password_conf" placeholder="rentrer le mot de pass.." required="required"  class="col-xs-12 col-md-10"/>
		</div>
		<div class="form-group row">
			<label for="" class="col-xs-12 col-md-2 identifie2">Lieu de naissance </label>
			<input type="text" name="lieu_de_naissance" id="Lieu de naissance" placeholder="Ex:Relizane" class="col-xs-12 col-md-10"required="required" />
		</div>
		<div class="form-group row">
        	<label for="niveau" class="col-xs-12 col-md-2">Niveau</label>
			<select name="niveau" id="niveau" class="select col-xs-12 col-md-10">
				<option value=""></option>
				<?php
					$niveaus_query = mysqli_query($con,"SELECT id, nom FROM niveau ORDER BY id ASC");
					while($niveau_row = mysqli_fetch_assoc($niveaus_query)){
						echo"<option value=".$niveau_row['id'].">".$niveau_row['nom']."</option>";
					}
				?>
			</select>
		</div>
		<div class="form-group row">
        	<label for="institut" class="col-xs-12 col-md-2">Institut</label>
			<select name="institut" id="institut" class="select col-xs-12 col-md-10">
				<option value=""></option>
				<?php
					$niveaus_query = mysqli_query($con,"SELECT id, nom FROM institut ORDER BY id ASC");
					while($niveau_row = mysqli_fetch_assoc($niveaus_query)){
						echo"<option value=".$niveau_row['id'].">".$niveau_row['nom']."</option>";
					}
				?>
			</select>
		</div>
		<div class="form-group row">
        	<label for="domaine" class="col-xs-12 col-md-2">Domaine</label>
			<select name="domaine" id="domaine" class="select col-xs-12 col-md-10">
				<option value=""></option>
				<?php
					$niveaus_query = mysqli_query($con,"SELECT id, nom FROM domaine ORDER BY id ASC");
					while($niveau_row = mysqli_fetch_assoc($niveaus_query)){
						echo"<option value=".$niveau_row['id'].">".$niveau_row['nom']."</option>";
					}
				?>
			</select>
		</div>
		<div class="form-group row">
        	<label for="filiere" class="col-xs-12 col-md-2">Filière</label>
			<select name="filiere" id="filiere" class="select col-xs-12 col-md-10">
				<option value="0"></option>
				<?php
					$niveaus_query = mysqli_query($con,"SELECT id, nom FROM filiere ORDER BY id ASC");
					while($niveau_row = mysqli_fetch_assoc($niveaus_query)){
						echo"<option value=".$niveau_row['id'].">".$niveau_row['nom']."</option>";
					}
				?>
			</select>
		</div>
		<div class="form-group row">
        	<label for="specialite" class="col-xs-12 col-md-2">Specialité</label>
			<select name="specialite" id="specialite" class="select col-xs-12 col-md-10">
				<option value="0"></option>
				<?php
					$niveaus_query = mysqli_query($con,"SELECT id, nom FROM specialite ORDER BY id ASC");
					while($niveau_row = mysqli_fetch_assoc($niveaus_query)){
						echo"<option value=".$niveau_row['id'].">".$niveau_row['nom']."</option>";
					}
				?>
			</select>
		</div>
		<div class="form-group row">
        	<input type="reset" value=" Actualiser" class="envoie" />
        	<input  type="button" id="submitStudentInfo" class="envoie" value=" Ajouter " />
		</div>
    </form>
    <script>
		$(document).ready(function(){
			$("#submitStudentInfo").on("click",function(){
				let nom = $("[name ='nom']").val();
				let prenom = $("[name ='prenom']").val();
				let student_id = $("[name ='student_id']").val();
				let date_de_naissance = $("[name ='date_de_naissance']").val();
				let adresse_mail = $("[name ='adresse_mail']").val();
				let password = $("[name ='password']").val();
				let password_conf = $("[name ='password_conf']").val();
				let lieu_de_naissance = $("[name ='lieu_de_naissance']").val();
				let niveau = $("[name ='niveau']").val();
				let domaine = $("[name ='domaine']").val();
				let institut = $("[name ='institut']").val();
				let specialite = $("[name ='specialite']").val();
				let filiere = $("[name ='filiere']").val();
			
				$.post("includes/cp/add_student_function.php",{
					nom: nom,
					prenom: prenom,
					student_id: student_id,
					date_de_naissance: date_de_naissance,
					adresse_mail: adresse_mail,
					password: password,
					password_conf: password_conf,
					lieu_de_naissance: lieu_de_naissance,
					niveau: niveau,
					domaine: domaine,
					institut: institut,
                    specialite: specialite,
					filiere: filiere
				},function(data,success){
					alert(data);
				});
			});
		});
	</script>