<?php require_once"includes/header.php"; ?>
        <title>Acueil</title>
    <style>
        .index-p{
            direction:rtl;
            font-size: 18px;
            font-family: Arial;
            text-align: center;
            font-weight: bold;
        }
    </style>
    </head>
    <body>
        <div class="container-fluid">
            <?php require_once"includes/menu.php"; ?>
            <div class="row" style="padding: 10px 10%; background-color:#efefef;">
            <div class="col-xs-12 col-md-8" style="padding: 0px 5px 0px 0px;">
                    <?php require_once"includes/slider.php"; ?>
                </div>
                <div class="col-xs-12 col-md-4" style="padding: 0px 0px 0px 5px;">
                    <div class="col-xs-12 col-md-12" style="padding: 0px 0px 5px 0px;">
                        <img src="img/1.png" style="height: 195px; width: 100%;">
                    </div>
                    <div class="col-xs-12 col-md-6" style="padding: 5px 5px 0px 0px;">
                        <img src="img/2.jpg" style="height: 195px; width: 100%;">
                    </div>
                    <div class="col-xs-12 col-md-6" style="padding: 5px 0px 0px 5px;">
                        <img src="img/3.jpg" style="height: 195px; width: 100%;">
                    </div>
                </div>
	        </div>
            <div class="row" style="padding: 10%; background-color:white;">
                <h1 style="text-align:center; background-color: #efefef;">Mot du directeur du Centre Universitaire Ahmed Zabana</h1>
                <h2 style="direction:rtl; text-align:center;">بسم الله الرحمن الرحيم</h2>
                <p class="index-p">

يسعدني أن أجعل من فاتحة هذه الكلمة الموجزة استهلالا أتوخى من خلاله مطاولة مقاصد القول المُحمَّلة بعبارات

الشكر والتقدير إلى شخص معالي وزير التعليم العالي والبحث العلمي وأن أثمِن عاليا ثقته التي وضعها في

شخصنا.
</p>
 
<p class="index-p">
وبعد...
</p>
<p class="index-p">
ما من شك، في أن الدولة الجزائرية قد وفرت للمركز الجامعي أحمد زبانة بغليزان كل مهيئات النهوض، التي تجعله يحتل مرتبة متقدمة في صُنّافة الجامعات عبر الوطن وخارجه وأن يتأوب موالج الأفق المنشود منه. ومن هنا، فإني أدعو الجميع إلى الاجتهاد في استحضار روح التآزر الواعية بروح المسؤولية الموكلة لها لتحقيق ذلك التَوَثب المرام من هذا الصرح العلمي.

وعلى الرغم من أني أعلم أنه لا يمكن لأي تجمع علمي وعملي بحجم مركزنا الجامعي أن ينأى عن عسر بعض التحديات والرهانات، لكني بالمقابل، على يقين تام أنه لو التزمنا بالتطبيق الصارم لجملة التعليمات والتوجيهات التي تسهر وزارتنا الوصية على تحديثها، وبتضافر جهود كل الفعاليات المتواجدة بالمركز سنتوق حتما إلى إزالة كل العقبات وتثبيط كل الصعوبات وندفع بالمركز إلى تلك المراقي المأمولة.

وضمن ثنايا هذه الكلمة مرة أخرى؛ أناشدكم جمعيا، طلبة وأساتذة وعمالا، للسير قدما في سبيل مواصلة جهودكم الفاعلة التي سبقتموني في بذلها، راجيا من المولى أن يوفقنا في إنجاح مهمتنا النبيلة، خدمة للجامعة الجزائرية ووطننا الحبيب.

</p>
<p class="index-p">
//*** المجد والخلود لشهدائنا الأبرار ***//
</p>
<p class="index-p">
السلام عليكم ورحمة الله.
</p>
<p class="index-p" style="background-color: #efefef;">
الدكتور بوعادي عابد
</p>   
            </div>
            <?php require_once"includes/footer.php"; ?>
        </div>
    </body>
</html>