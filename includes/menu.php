<div class="row" style="padding: 0% 10%; background-color: white;">
	<div class="col-xs-6 col-md-3" style="padding: 0px;">
		<img src="img/7.png" style="height: 150px; width:100%;margin-top: 50px;"/>
	</div>
	<div class="col-xs-12 col-md-9" style="padding: 0px;">
		<img src="img/6.jpg" style="height: 200px; width:100%;"/>
	</div>  
</div>
<div class="row" style="padding: 0% 10%; background-color:white;">
	<nav class="navbar navbar-inverse" style="margin-bottom: 0px; border-radius: 0px;">
	<div class="container-fluid">
		<div class="navbar-header">
		<a class="navbar-brand" href="#">CUR</a>
		</div>
		<ul class="nav navbar-nav">
			<li class="active">
			<a href="index.php"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;Accueil</a>
			</li>
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Espaces
				<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="student.php">Etudiant</a></li>
					<li><a href="teacher.php">Enseignant</a></li>
					<li><a href="admin.php">Administrateur</a></li>
				</ul>
			</li>
			<li><a href="formation.php">Formations</a></li>
			<li><a href="#">Affichage</a></li>
			<li><a href="#">Aide</a></li>
			
			<?php
			if(!$connected){
				echo'<li><a data-toggle="modal" data-target="#loginModal" style="cursor: pointer;">Login</a></li>';
			}else{
				echo $user_name;
				echo'<li><a href="logout.php">Logout</a></li>';
			}
			?>
			
		</ul>
	</div>
	</nav>
</div>
<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Log in</h4>
        </div>
        <form action="login.php" method="post" name="login">
			<div class="modal-body">
			<div class="form-group">
					<label for="user_type">Role</label>
					<select name="user_type" id="user_type">
						<option value="student">Etudiant</option>
						<option value="teacher">Enseignant</option>
						<option value="admin">Administrateur</option>
					</select>
				</div>
			<div class="form-group">
					<label for="email">Email</label>
					<input type="text" name="email"  class="form-control" id="email" aria-describedby="usernameHelp" placeholder="Email">
				</div>
				<div class="form-group">
					<label for="password">Mot de pass</label>
					<input type="password" name="password_m" id="password"  class="form-control" aria-describedby="emailHelp" placeholder="mot de pass">
				</div>
				
			</div>
			<div class="modal-footer">
					<button type="submit" class="btn btn-sm btn-success">Login</button>
			</div>
		</form>
      </div>
      
    </div>
  </div>