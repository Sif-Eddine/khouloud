<?php ob_start();
session_start();
$connected = false;
require_once("includes/config.php");
if(!empty($_SESSION["email"]) && !empty($_SESSION["user_type"])){
	$user_type = $_SESSION["user_type"];
	$user_email = $_SESSION["email"];
    if($user_type == "student"){ $bd_table = "etudiant";}
    else if($user_type == "teacher"){ $bd_table = "enseignant";}
    else if($user_type == "admin"){ $bd_table = "administrateur";}
	$user_info_query = mysqli_query($con, "SELECT * FROM $bd_table WHERE email='$user_email'");
	$check_connected_user_existance = mysqli_num_rows($user_info_query);
	if($check_connected_user_existance == 1){
		$connected = true;
		$user_info_row = mysqli_fetch_assoc($user_info_query);
		$user_id = $user_info_row["id"];
		$user_name = $user_info_row["nom"];
		$user_firstname = $user_info_row["prenom"];
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/bootstrap-social.css"/>
	<link rel="stylesheet" href="css/bootstrap-theme.min.css"/>
	<link rel="stylesheet" href="css/font-awesome.min.css"/>
    <script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/custom.css">