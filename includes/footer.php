    <style>
        .footer-contact{ border-right: solid 1px #2C2C2C;}
        footer{font-size: 14px; color: white;}
    </style>
    <script type="text/javascript" src="js/printThis.js"></script>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });
    </script>
    <div class="row" style="padding: 20px 10%; background-color: #222222; border-top: solid 5px #E5E5E5;">s
    <footer class='row'>
        <div class="col-xs-12 col-md-3 footer-contact">
            <h6 class="footer-section-title">Contacter nous:</h6>
            <ul>
                <li><b>Adress: </b><a href="https://goo.gl/maps/LRyacfsF1P94GT3y8" target="_blank">Bermadia, Relizane</a></li>
                <li><b> Tel: </b>046 22 12 90</li>
                <li><b>Email: </b><a href="mailto:admin@cu-relizane.dz" target="_blank">admin@cu-relizane.dz</a></li>
            </ul>
        </div>
        <div class="col-xs-6 col-md-3 footer-contact">
            <h6 class="footer-section-title">Liens:</h6>
            <ul>
                <li><a href='index.php'><span class="glyphicon glyphicon-home" aria-hidden="true"></span> ACCEUIL</a></li>
                <li><a href='apropos.php'><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> A PROPOS</a></li>
                <li><a href='contact.php'><span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span> CONTACT</a></li>
            </ul>
        </div>
        <div class="col-xs-6 col-md-2 footer-contact">
            <h6 class="footer-section-title">Media Sociaux:</h6>
            <a class="btn btn-social-icon btn-google-plus" href="https://plus.google.com"><i class="fa fa-google-plus"></i></a>
            <a class="btn btn-social-icon btn-facebook" href="https://www.facebook.com"><i class="fa fa-facebook"></i></a>
            <a class="btn btn-social-icon btn-twitter" href="https://twitter.com"><i class="fa fa-twitter"></i></a>
        </div>
        <div class="col-xs-6 col-md-4 footer-contact">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4581.870769475177!2d0.5778361035593873!3d35.70477001632719!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12819d0e0d704395%3A0xf9120d6917fd7449!2sCentre%20Universitaire%20Relizane!5e0!3m2!1sen!2sdz!4v1598902913364!5m2!1sen!2sdz" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    </footer>
</div>
<?php ob_flush();