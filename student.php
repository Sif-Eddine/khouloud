<?php require_once"includes/header.php"; ?>
        <title>Acueil</title>
    <style>
        .index-p{
            direction:rtl;
            font-size: 18px;
            font-family: Arial;
            text-align: center;
            font-weight: bold;
        }
        .file_title:hover{
            background-color: #eee;
        }
    </style>
    </head>
    <body>
        <div class="container-fluid">
            <?php require_once"includes/menu.php"; ?>
            <div class="row" style="padding: 20px 10%; background-color: white;">
                <?php
                    if(!empty($_POST["title"])){
                        $file_name = $_FILES['course']['name'];
                        $file_size = $_FILES['course']['size'];
                        $file_type = $_FILES['course']['type'];
                        $course_type = $_POST['course_type'];
                        $file_tmp = $_FILES['course']['tmp_name'];
                        $destination = "upload/courses/";
                        $title = mysqli_real_escape_string($con, $_POST["title"]);
                        $module_id = $_POST["module_id"];
                        $uploader_id = $user_id;
                        $error_repport = "";
                        $error_counter = 0;
                        $module_name = mysqli_fetch_assoc(mysqli_query($con, "SELECT nom FROM module WHERE id='".$module_id."'"));
                        $module_name = $module_name["nom"];
                        $file_size_limit = 20; // l'unité est MO    1024 O = 1KO // 1024 KO = 1 MO
                        if($file_size>(1024*1024*$file_size_limit)){
                            $error_repport = $error_repport." le volume de fichier est superieur à ".$file_size_limit."MO<br />";
                            $error_counter++;
                        }
                        if($file_type!='image/jpg' && $file_type!='image/jpeg' && $file_type!='image/bmp' && $file_type!='image/png' && $file_type!='image/gif' && $file_type!='application/pdf' && $file_type!='text/plain' && $file_type!='application/vnd.openxmlformats-officedocument.wordprocessingml.document' && $file_type!='application/msword'){
                            $error_repport = $error_repport."le type de fichier unacceptable!<br />";
                            $error_counter++;
                        }
                        if(file_exists($destination.$_FILES["course"]["name"])){
                            $error_repport = $error_repport."Ce fichier existe déja!<br />";
                            $error_counter++;
                        }
                        if(empty($file_name)){
                            $error_repport .= "Vous n'avez pas selectionnez un fichier!";
                            $error_counter++;
                        }
                        if($error_counter == 0){
                            $new_filename = $user_id.$time.$file_name;
                            if(move_uploaded_file($file_tmp,$destination.$new_filename)){
                                $url = $destination.$new_filename;
                            }
                            if(mysqli_query($con, "INSERT INTO `cours` (`id`, `title`, `module_id`, `uploader_id`, `course_type`, `url`, `time`) VALUES (NULL, '$title', '$module_id', '$uploader_id', '$course_type', '$url', '$time')")){
                                echo"le ".$course_type." de module ".$module_name." a était ajouté avec succée<br />";
                            }
                        }
                        else {echo$error_repport."<br /><br /><br />";}
                    }
                ?>


                
                <!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">AJOUTER UN FICHIER</h4>
                            </div>
                            <div class="modal-body">
                                <div class="col-xs-12">
                                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-xs-4" for="title">Titre de fichier:</label>
                                            <input class="col-md-8" type="text" id="title" name="title" placeholder="Entrer le titre de cours"/>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-4" for="module_id">Module:</label>
                                            <select class="col-md-8" id="module_id" name="module_id"> 
                                                <?php
                                                    $courses = mysqli_query($con, "SELECT * FROM module WHERE teacher='$user_id'");
                                                    while($course = mysqli_fetch_assoc($courses)){
                                                        
                                                        echo"<option value='".$course['id']."'>".$course['nom']."</option>";
                                                    }
                                                ?>  
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="course" class="col-xs-4">Fichier:</label> 
                                            <input class="col-md-8" type="file" id="course" name="course" accept=".pdf,.doc,.docx,.txt"/>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-4" for="course_type">Type de fichier:</label> 
                                            <select class="col-md-8" id="course_type" name="course_type">
                                                <option value="cours">COURS</option>
                                                <option value="td">TD</option>
                                                <option value="tp">TP</option>
                                                <option value="announcement">ANNONCE</option>
                                                <option value="exam_solution">Correction d'examen</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" id="submit_course" class="btn btn-success" value="Déposer">
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="modal-footer">
                            
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row" style="padding: 20px 10%; background-color: #efefef;">
                	
                                                    
                    <div class="col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="text-align: center; font-weight: bold;">LE PANNEAU D'AFFICHAGE</div>
                            <div class="panel-body" style="background-image: url('img/blackboard.jpg'); width: 100%; height: 400px; background-size: 100% 400px;">
                                <?php require_once"includes/an_slider.php"; ?>
                            </div>
                            <div class="panel-footer"></div>
                        </div>
                    </div>    
            </div>

            <div class="row" style="padding: 10px 10%; background-color:white;">                        
                <div class="col-xs-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home">MODULES</a></li>
                        <li><a data-toggle="tab" href="#menu1">ANNONCES</a></li>
                        <li><a data-toggle="tab" href="#menu2">PLANNING DES EXAMANS</a></li>
                        <li><a data-toggle="tab" href="#menu3">EMPLOI DU TEMP</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                            <div class="panel-group" id="faqAccordion">
                                <?php
                                $modules = mysqli_query($con, "SELECT * FROM module WHERE teacher='$user_id'");
                                $counter = 0;
                                while($module = mysqli_fetch_assoc($modules)){
                                    $counter ++;
                                    $module_id = $module['id'];
                                    $module_name = $module['nom'];
                                    echo'
                                        <div class="panel panel-default">
                                            <div class="panel-heading accordion-toggle question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question'.$counter.'">
                                                <h4 class="panel-title">
                                                    <a class="ing">'.$module_name.'</a>
                                                </h4>
                                            </div>
                                            <div id="question'.$counter.'" class="panel-collapse collapse" style="height: 0px;">
                                                <div class="panel-body">
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading">COURS</div>
                                                        <div class="panel-body">
                                                            <table class="table">';

                                                                $files = mysqli_query($con, "SELECT * FROM cours WHERE uploader_id='$user_id' AND module_id='$module_id' AND course_type='cours' ORDER BY time ASC");
                                                                while($file = mysqli_fetch_assoc($files)){
                                                                    $title = $file['title'];
                                                                    $id = $file['id'];
                                                                    $url = $file['url'];
                                                                    echo"
                                                                    <tr class='file_title'>
                                                                        <td>
                                                                            $title
                                                                        </td>
                                                                        <td>
                                                                            
                                                                            <a target='_blank' href='".$url."'  id='download_$id' name='download_file' style='float: right;' class='btn btn-success btn-sm glyphicon glyphicon-save' data-toggle='tooltip' title='Télécharger le fichier'></a>
                                                                            
                                                                        </td>
                                                                    </tr>";    
                                                                }
                                                            echo'
                                                            </table>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="panel panel-success">
                                                        <div class="panel-heading">TD</div>
                                                        <div class="panel-body">
                                                            <table class="table">';
                                                                $files = mysqli_query($con, "SELECT * FROM cours WHERE uploader_id='$user_id' AND module_id='$module_id' AND course_type='td' ORDER BY time ASC");
                                                                while($file = mysqli_fetch_assoc($files)){
                                                                    $title = $file['title'];
                                                                    $id = $file['id'];
                                                                    $url = $file['url'];
                                                                    echo"
                                                                    <tr class='file_title'>
                                                                        <td>
                                                                            $title
                                                                        </td>
                                                                        <td>
                                                                            
                                                                            <a target='_blank' href='".$url."'  id='download_$id' name='download_file' style='float: right;' class='btn btn-success btn-sm glyphicon glyphicon-save' data-toggle='tooltip' title='Télécharger le fichier'></a>
                                                                            
                                                                        </td>
                                                                    </tr>";    
                                                                }
                                                            echo'
                                                            </table>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="panel panel-danger">
                                                        <div class="panel-heading">TP</div>
                                                        <div class="panel-body">
                                                            <table class="table">';
                                                                $files = mysqli_query($con, "SELECT * FROM cours WHERE uploader_id='$user_id' AND module_id='$module_id' AND course_type='tp' ORDER BY time ASC");
                                                                while($file = mysqli_fetch_assoc($files)){
                                                                    $title = $file['title'];
                                                                    $id = $file['id'];
                                                                    $url = $file['url'];
                                                                    echo"
                                                                    <tr class='file_title'>
                                                                        <td>
                                                                            $title
                                                                        </td>
                                                                        <td>
                                                                            
                                                                            <a target='_blank' href='".$url."'  id='download_$id' name='download_file' style='float: right;' class='btn btn-success btn-sm glyphicon glyphicon-save' data-toggle='tooltip' title='Télécharger le fichier'></a>
                                                                            
                                                                        </td>
                                                                    </tr>";    
                                                                }
                                                            echo'
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-info">
                                                        <div class="panel-heading">CORRECTION D\'EXAMEN</div>
                                                        <div class="panel-body">
                                                            <table class="table">';
                                                                $files = mysqli_query($con, "SELECT * FROM cours WHERE uploader_id='$user_id' AND module_id='$module_id' AND course_type='exam_solution' ORDER BY time ASC");
                                                                while($file = mysqli_fetch_assoc($files)){
                                                                    $title = $file['title'];
                                                                    $id = $file['id'];
                                                                    $url = $file['url'];
                                                                    echo"
                                                                    <tr class='file_title'>
                                                                        <td>
                                                                            $title
                                                                        </td>
                                                                        <td>
                                                                            
                                                                            <a target='_blank' href='".$url."'  id='download_$id' name='download_file' style='float: right;' class='btn btn-success btn-sm glyphicon glyphicon-save' data-toggle='tooltip' title='Télécharger le fichier'></a>
                                                                            
                                                                        </td>
                                                                    </tr>";    
                                                                }
                                                            echo'
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ';
                                }
                                ?>
                            </div>
                            <!--/panel-group-->
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <h3>Menu 1</h3>
                            <p>Some content in menu 1.</p>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            <h3>Menu 2</h3>
                            <p>Some content in menu 2.</p>
                        </div>
                        <div id="menu3" class="tab-pane fade">
                            <h3>Menu 3</h3>
                            <p>Some content in menu 3.</p>
                        </div>
                    </div>
                </div>
                <!--
                <div class="col-xs-3">
                    <a href="teacher.php?section=cours_upload">
                        <div class="panel panel-success">
                            <div class="panel-heading" style="text-align: center; font-weight: bold;">COURS</div>
                            <div class="panel-body">
                                <img src="img/cours.jpg" style="width:100%;height: 200px;">
                            </div>
                            <div class="panel-footer"></div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-3">
                    <a href="includes/teacher_p/cours_upload.php">
                        <div class="panel panel-warning">
                            <div class="panel-heading" style="text-align: center; font-weight: bold;">TD / TP</div>
                            <div class="panel-body">
                                <img src="img/td.jpg" style="width:100%;height: 200px;">
                            </div>
                            <div class="panel-footer"></div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-3">
                <a href="includes/teacher_p/cours_upload.php">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-align: center; font-weight: bold;">EMPLOI DU TEMP</div>
                        <div class="panel-body">
                            <img src="img/emploi.jpg" style="width:100%;height: 200px;">
                        </div>
                        <div class="panel-footer"></div>
                    </div>
                    </a>
                </div>
                <div class="col-xs-3">
                <a href="includes/teacher_p/cours_upload.php">
                    <div class="panel panel-danger">
                        <div class="panel-heading" style="text-align: center; font-weight: bold;">PLANNING DES EXAMANS</div>
                        <div class="panel-body">
                            <img src="img/exam.jpg" style="width:100%;height: 200px;">
                        </div>
                        <div class="panel-footer"></div>
                    </div>
                    </a>
                </div>-->
	            </div>
            </div>
            <?php require_once"includes/footer.php"; ?>
        </div>
    </body>
</html>