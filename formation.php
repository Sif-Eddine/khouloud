<?php require_once"includes/header.php"; ?>
        <title>Acueil</title>
    <style>
        .index-p{
            direction:rtl;
            font-size: 18px;
            font-family: Arial;
            text-align: center;
            font-weight: bold;
        }
    </style>
    </head>
    <body>
        <div class="container-fluid">
            <?php require_once"includes/menu.php"; ?>
            <div class="row" style="padding: 10%; background-color:white;">
                <div class="col-xs-12">
                    <h2 style="background-color: #efefef; text-align: center; font-weight: bold;">LICANCE</h2>
                </div>
                <div class="col-xs-12 col-md-8 col-md-push-2">
                    <img src="img/formations/SpcialitsOuvertesLicence1.png" style="width: 100%;">
                </div>
                <div class="col-xs-12 col-md-8 col-md-push-2">
                    <img src="img/formations/SpcialitsOuvertesLicence2.png" style="width: 100%;">
                </div>
                <div class="col-xs-12 col-md-8 col-md-push-2">
                    <img src="img/formations/SpcialitsOuvertesLicence3.png" style="width: 100%;">
                </div>
                <div class="col-xs-12">
                    <h2 style="background-color: #efefef; text-align: center; font-weight: bold; margin-top: 20px;">MASTER</h2>
                </div>
                <div class="col-xs-12 col-md-8 col-md-push-2">
                    <img src="img/formations/SpcialitsOuvertesMaster1.png" style="width: 100%;">
                </div>
                <div class="col-xs-12 col-md-8 col-md-push-2">
                    <img src="img/formations/SpcialitsOuvertesMaster2.png" style="width: 100%;">
                </div>
                <div class="col-xs-12">
                    <h2 style="background-color: #efefef; text-align: center; font-weight: bold; margin-top: 20px;">DOCTORAT</h2>
                </div>
                <div class="col-xs-12 col-md-8 col-md-push-2">
                    <img src="img/formations/Doctorat.png" style="width: 100%;">
                </div> 
            </div>
            <?php require_once"includes/footer.php"; ?>
        </div>
    </body>
</html>