-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 10, 2020 at 12:14 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrateur`
--

CREATE TABLE `administrateur` (
  `id` int(12) NOT NULL,
  `matricule` varchar(12) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `date_naissance` date NOT NULL,
  `lieu_naissance` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `telephone` int(11) NOT NULL,
  `password` varchar(250) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `administrateur`
--

INSERT INTO `administrateur` (`id`, `matricule`, `nom`, `prenom`, `date_naissance`, `lieu_naissance`, `email`, `telephone`, `password`, `time`) VALUES
(1, '0', 'meliani ', 'khalida', '1997-06-16', 'relizane ', 'melianikhalida@gmail.com', 791915522, '', 0),
(3, '0', 'meliani ', 'fatima ', '1993-03-05', 'relizane ', 'melianifatima@gmail.com', 791115501, '', 0),
(4, '0', 'yechba ', 'amina ', '1989-01-12', 'relizane ', 'yechbaamina@gmail.com', 781456599, '', 0),
(5, '0', 'yechba ', 'khalida', '1997-09-16', 'relizane ', 'yechbakhalida@gmail.com', 556455822, '', 0),
(6, '0', 'meliani ', 'khalida', '2020-07-23', 'relizane ', 'melianikhalida@gmail.com', 781456599, '', 0),
(7, '0', 'djalali', 'yara', '1998-07-03', 'tiaret', 'djalaliyara@gmail.com', 781514599, '', 0),
(8, '0', 'Belhachmi', 'nessrinee', '1998-01-23', 'oran', 'belhachminessrine@gmail.com', 555016411, '', 0),
(9, '0', 'dude', 'ddd', '2000-05-11', 'scscscsc', 'rrf@rien.com', 0, '', 0),
(10, '0', 'dude', 'ddd', '2000-05-11', 'scscscsc', 'rrf@rien.com', 0, '', 0),
(11, '0', 'meliani ', 'khalida', '2020-07-10', 'tiaret', 'melianikhalida@gmail.com', 791915522, '', 0),
(14, '555555555555', 'meliani', 'lamia', '1990-07-14', 'relizane', 'Amelamel@gmail.com', 781455166, 'e719b97358a676ad529e8970a7de311a', 1595789480),
(15, '666666669874', 'belalia', 'sif eddine', '1992-07-08', 'relizane', 'belaliasifeddine@gmail.com', 512121212, '25f9e794323b453885f5181f1b624d0b', 1595789521),
(16, '954125874562', 'nasri', 'nasri', '1988-05-01', 'relizane', 'amelamel@gmail.com', 781455166, '25f9e794323b453885f5181f1b624d0b', 1596729364),
(17, '101010101010', 'sssss', 'ddddd', '1998-06-16', 'fgfffff', 'gg@fgg.gg', 505050505, '25f9e794323b453885f5181f1b624d0b', 1599000207);

-- --------------------------------------------------------

--
-- Table structure for table `canvas`
--

CREATE TABLE `canvas` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `specialite` int(11) NOT NULL,
  `starting_time` int(11) NOT NULL,
  `ending_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cours`
--

CREATE TABLE `cours` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `module_id` int(11) NOT NULL,
  `uploader_id` int(11) NOT NULL,
  `course_type` varchar(20) NOT NULL,
  `url` varchar(200) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cours`
--

INSERT INTO `cours` (`id`, `title`, `module_id`, `uploader_id`, `course_type`, `url`, `time`) VALUES
(1, '', 0, 0, '', '', 0),
(2, 'مقدمة ابن خلدون', 3, 14, 'tp', 'upload/courses/141599670130Doc4.docx', 1599670130),
(3, 'Map with Marker', 3, 14, 'cours', 'upload/courses/141599672368img20200805_21233358.pdf', 1599672368),
(4, 'ddddss', 3, 14, 'cours', 'upload/courses/141599672613دراجة نارية.docx', 1599672613);

-- --------------------------------------------------------

--
-- Table structure for table `domaine`
--

CREATE TABLE `domaine` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `institut` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `domaine`
--

INSERT INTO `domaine` (`id`, `nom`, `institut`) VALUES
(1, 'st', 0),
(2, 'mi', 0),
(3, 'arabe', 0),
(4, 'francais', 0);

-- --------------------------------------------------------

--
-- Table structure for table `emploi_du_temp`
--

CREATE TABLE `emploi_du_temp` (
  `id` int(11) NOT NULL,
  `salle` varchar(30) NOT NULL,
  `jour` date NOT NULL,
  `heur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `enseignant`
--

CREATE TABLE `enseignant` (
  `id` int(12) NOT NULL,
  `matricule` varchar(13) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `date_naissance` date NOT NULL,
  `lieu_naissance` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telephone` varchar(11) NOT NULL,
  `grade` int(30) NOT NULL,
  `institut` int(11) NOT NULL,
  `specialite` tinyint(4) NOT NULL,
  `filiere` tinyint(4) NOT NULL DEFAULT 0,
  `domaine` tinyint(4) NOT NULL,
  `password` varchar(250) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `enseignant`
--

INSERT INTO `enseignant` (`id`, `matricule`, `nom`, `prenom`, `date_naissance`, `lieu_naissance`, `email`, `telephone`, `grade`, `institut`, `specialite`, `filiere`, `domaine`, `password`, `time`) VALUES
(3, '0', 'yechba', 'khadidja', '2000-08-01', 'relizane', 'yechbakhadija@gmail.com', '555048818', 2, 2, 2, 2, 1, '', 0),
(4, '0', 'yechba', 'khadidja', '2000-08-01', 'relizane', 'yechbakhadija@gmail.com', '555048818', 3, 2, 2, 2, 1, '', 0),
(5, '0', 'yechba', 'khadidja', '2000-08-01', 'relizane', 'yechbakhadija@gmail.com', '781456851', 3, 2, 2, 3, 1, '', 0),
(9, '123589746324', 'yechba', 'khadidja', '2000-08-01', 'relizane', 'yechbakhadija@gmail.com', '0781455166', 2, 2, 4, 1, 1, '', 1596724568),
(10, '159159159123', 'yechba', 'khadidja', '2000-08-01', 'relizane', 'yechbakhadija@gmail.com', '0512121212', 2, 2, 3, 3, 1, '', 1596738001),
(11, '010101010101', 'dddd', 'dddd', '1994-06-23', 'fgfffff', 'gg@fgg.gg', '0555555555', 1, 1, 0, 0, 1, '25f9e794323b453885f5181f1b624d0b', 1597790195),
(14, '101010101012', 'king', 'kong', '1970-12-22', 'USA', 'kingkong@gmail.com', '0502020201', 5, 1, 0, 0, 1, '25f9e794323b453885f5181f1b624d0b', 1599607081);

-- --------------------------------------------------------

--
-- Table structure for table `etudiant`
--

CREATE TABLE `etudiant` (
  `id` int(11) NOT NULL,
  `matricule` varchar(30) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `date_naissance` date NOT NULL,
  `lieu_naissance` varchar(30) NOT NULL,
  `niveau` int(11) NOT NULL,
  `institut` int(11) NOT NULL,
  `domaine` tinyint(4) NOT NULL,
  `filiere` tinyint(4) NOT NULL DEFAULT 0,
  `specialite` tinyint(4) NOT NULL DEFAULT 0,
  `telephone` varchar(10) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(250) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `etudiant`
--

INSERT INTO `etudiant` (`id`, `matricule`, `nom`, `prenom`, `date_naissance`, `lieu_naissance`, `niveau`, `institut`, `domaine`, `filiere`, `specialite`, `telephone`, `email`, `password`, `time`) VALUES
(1, '0', 'Belkacem', 'youcef', '2020-07-15', 'relizane', 1, 0, 0, 0, 0, '', '', '', 0),
(2, '0', 'nasri', 'lamia', '2018-07-12', 'relizane', 1, 0, 0, 0, 0, '', '', '', 0),
(3, '0', 'nasri', 'lamia', '2020-07-31', 'relizane', 1, 0, 0, 0, 0, '', '', '', 0),
(4, '0', 'yettou', 'lamia', '1997-01-29', 'relizane', 1, 0, 0, 0, 0, '', '', '', 0),
(5, '0', 'nasri', 'lamia', '1998-05-01', 'relizane', 1, 0, 0, 0, 0, '', '', '', 0),
(6, '0', 'meliani', 'lamia', '2000-07-28', 'oran ', 1, 1, 1, 1, 3, '', 'belaliasifeddine@gmail.com', '25f9e794323b453885f5181f1b624d0b', 0),
(7, '0', 'djelali', 'hayat', '2018-07-13', 'alger', 1, 0, 0, 0, 0, '', '', '', 0),
(8, '0', 'nasri', 'lamia', '2020-07-09', 'relizane', 1, 0, 0, 0, 0, '', '', '', 0),
(9, '0', 'nasri', 'lamia', '2020-07-15', 'relizane', 1, 0, 0, 0, 0, '', '', '', 0),
(11, '10201000', 'beljilali', 'jilali', '1991-01-03', 'oran', 1, 0, 0, 0, 0, '', '', '', 0),
(13, '123456789789', 'belalia', 'dddddd', '1995-09-04', 'tiaret', 1, 0, 0, 0, 0, '', 'boomboom@gmail.com', '', 0),
(15, '123456789101', 'nasri', 'cscsc', '1988-07-05', 'ssfsddsd', 1, 0, 0, 0, 0, '', 'lamialamia@gmail.com', '25f9e794323b453885f5181f1b624d0b', 1595536927),
(16, '123456789798', 'nasri', 'lamia', '1998-06-10', 'relizane', 1, 0, 0, 0, 0, '', 'Amelamel@gmail.com', '25f9e794323b453885f5181f1b624d0b', 1595537168),
(17, '000000000000', 'dddd', 'ssss', '2000-12-12', 'sasas', 1, 1, 1, 2, 12, '', 'ss@ss.ss', '25f9e794323b453885f5181f1b624d0b', 1595981318),
(18, '123456789103', 'sasasas', 'azeeee', '2000-11-20', 'red', 7, 1, 2, 4, 0, '', 'belz@ff.cd', '25f9e794323b453885f5181f1b624d0b', 1596578328),
(19, '147852369156', 'lamia', 'lamia', '1998-05-01', 'relizane', 3, 1, 1, 1, 4, '', 'lamialamia@gmail.com', '25f9e794323b453885f5181f1b624d0b', 1596722606),
(20, '852147963658', 'yettou', 'nessrine', '1998-05-01', 'relizane', 1, 2, 4, 7, 4, '', 'Amelamel@gmail.com', 'f5bb0c8de146c67b44babbf4e6584cc0', 1596737843),
(21, '010101010101', 'dddd', 'dddd', '1990-02-22', 'fgfffff', 8, 2, 4, 8, 4, '0202020202', 'gg@fgg.gg', '25f9e794323b453885f5181f1b624d0b', 1598998707);

-- --------------------------------------------------------

--
-- Table structure for table `examen`
--

CREATE TABLE `examen` (
  `id` int(11) NOT NULL,
  `module` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `filiere`
--

CREATE TABLE `filiere` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `domaine` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `filiere`
--

INSERT INTO `filiere` (`id`, `nom`, `domaine`) VALUES
(1, 'chimie', 0),
(2, 'GC', 0),
(3, 'math', 0),
(4, 'informatique', 0),
(5, 'arabe1', 0),
(6, 'arabe2', 0),
(7, 'francais1', 0),
(8, 'francais2', 0);

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

CREATE TABLE `grade` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `grade`
--

INSERT INTO `grade` (`id`, `nom`) VALUES
(1, 'MAB'),
(2, 'MAA'),
(3, 'MCB'),
(4, 'MCA'),
(5, 'Professeur');

-- --------------------------------------------------------

--
-- Table structure for table `institut`
--

CREATE TABLE `institut` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `institut`
--

INSERT INTO `institut` (`id`, `nom`) VALUES
(1, 'st'),
(2, 'les langues');

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `canvas` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `teacher` int(11) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `nom`, `canvas`, `year`, `teacher`, `time`) VALUES
(3, 'math 5', 0, 2011, 14, 1111),
(4, 'physic 2', 0, 2011, 14, 2222);

-- --------------------------------------------------------

--
-- Table structure for table `niveau`
--

CREATE TABLE `niveau` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `niveau`
--

INSERT INTO `niveau` (`id`, `nom`) VALUES
(1, 'Licence 1'),
(2, 'Licence 2'),
(3, 'Licence 3'),
(4, 'Master 1'),
(5, 'Master 2'),
(6, 'Doctorat 1'),
(7, 'Doctorat 2'),
(8, 'Doctorat 3');

-- --------------------------------------------------------

--
-- Table structure for table `note`
--

CREATE TABLE `note` (
  `id_n` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `id_m` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `salle`
--

CREATE TABLE `salle` (
  `numero_s` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `specialite`
--

CREATE TABLE `specialite` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `filiere` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `specialite`
--

INSERT INTO `specialite` (`id`, `nom`, `filiere`) VALUES
(3, 'informatique indistrual', 1),
(4, 'chimie organique', 2);

-- --------------------------------------------------------

--
-- Table structure for table `td`
--

CREATE TABLE `td` (
  `id_td` int(11) NOT NULL,
  `intitulé_tp` varchar(30) NOT NULL,
  `module_td` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tp`
--

CREATE TABLE `tp` (
  `id_tp` int(11) NOT NULL,
  `intitulé_tp` varchar(30) NOT NULL,
  `module_tp` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrateur`
--
ALTER TABLE `administrateur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `canvas`
--
ALTER TABLE `canvas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cours`
--
ALTER TABLE `cours`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domaine`
--
ALTER TABLE `domaine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emploi_du_temp`
--
ALTER TABLE `emploi_du_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enseignant`
--
ALTER TABLE `enseignant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `etudiant`
--
ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `niveau` (`niveau`);

--
-- Indexes for table `examen`
--
ALTER TABLE `examen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `filiere`
--
ALTER TABLE `filiere`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grade`
--
ALTER TABLE `grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institut`
--
ALTER TABLE `institut`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `niveau`
--
ALTER TABLE `niveau`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `note`
--
ALTER TABLE `note`
  ADD PRIMARY KEY (`id_n`),
  ADD KEY `id` (`id`),
  ADD KEY `id_m` (`id_m`);

--
-- Indexes for table `salle`
--
ALTER TABLE `salle`
  ADD PRIMARY KEY (`numero_s`);

--
-- Indexes for table `specialite`
--
ALTER TABLE `specialite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `td`
--
ALTER TABLE `td`
  ADD PRIMARY KEY (`id_td`);

--
-- Indexes for table `tp`
--
ALTER TABLE `tp`
  ADD PRIMARY KEY (`id_tp`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrateur`
--
ALTER TABLE `administrateur`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `canvas`
--
ALTER TABLE `canvas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cours`
--
ALTER TABLE `cours`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `domaine`
--
ALTER TABLE `domaine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `emploi_du_temp`
--
ALTER TABLE `emploi_du_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `enseignant`
--
ALTER TABLE `enseignant`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `etudiant`
--
ALTER TABLE `etudiant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `examen`
--
ALTER TABLE `examen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `filiere`
--
ALTER TABLE `filiere`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `grade`
--
ALTER TABLE `grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `institut`
--
ALTER TABLE `institut`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `niveau`
--
ALTER TABLE `niveau`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `note`
--
ALTER TABLE `note`
  MODIFY `id_n` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salle`
--
ALTER TABLE `salle`
  MODIFY `numero_s` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `specialite`
--
ALTER TABLE `specialite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `td`
--
ALTER TABLE `td`
  MODIFY `id_td` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tp`
--
ALTER TABLE `tp`
  MODIFY `id_tp` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `note_ibfk_1` FOREIGN KEY (`id`) REFERENCES `etudiant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `note_ibfk_2` FOREIGN KEY (`id_m`) REFERENCES `module` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
