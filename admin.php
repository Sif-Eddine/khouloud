        <?php require_once"includes/header.php";
			//if($user_type != "admin" AND $user_type!="teacher"){exit('<meta http-equiv="refresh" content="0; url=index.php">');}
		?>
        <title>Page d'administation</title>
        <style>
	.sidenav {
		height: 100%;
		width: 160px;
		position: fixed;
		z-index: 1;
		top: 0;
		left: 0;
		background-color: #111;
		overflow-x: hidden;
		padding-top: 20px;
	}

	.sidenav a {
		padding: 6px 8px 6px 16px;
		text-decoration: none;
		/* font-size: 25px; */
		color: #818181;
		display: block;
		border-bottom: solid 1px #191b1a;
	}

	.sidenav a:hover {
		color: #f1f1f1;
	}

	.main {
		margin-left: 160px; /* Same as the width of the sidenav */
		/* font-size: 28px; Increased text to enable scrolling */
		padding: 0px 10px;
	}

	@media screen and (max-height: 450px) {
	.sidenav {padding-top: 15px;}
	}
	.selectiveTable{
		border-bottom: solid 1px #eee;
		font: 12px;		
	}
	.selectiveTable:hover{
		background-color: #eee;
	}
	.nav a{color: #fff;}
	.nav a:hover{color: orange;}
</style>
    </head>
    <body>
        <div class="container-fluid">
				<?php require_once"includes/menu.php"; ?>
				<div class="row"style="padding: 0px 10%; background-color: white;">
					<div class="col-xs-12">
					<ol class="breadcrumb breadcrumb-arrow">
						<li><a href="/index.php">Acceuil</a></li>
						<li><a href="/admin.php?section=administrators_list">Espace administrateur</a></li>
						<?php

						?>
						<li class="active"><span>Liste des ensegniants</span></li>
					</ol>
					</div>
				</div>
            <div class="row" style="padding: 20px 10%; background-color: white;">
                	
                <div class="col-xs-12">
                    <?php
						if(isset($_GET['section']) and $_GET['section']=='students_list'){require_once"includes/cp/students_list.php";}
						else if(isset($_GET['section']) and $_GET['section']=='teachers_list'){require_once"includes/cp/teachers_list.php";}
						else if(isset($_GET['section']) and $_GET['section']=='administrators_list'){require_once"includes/cp/administrators_list.php";}
						else{
							echo'
							<div class="col-xs-3">
				<a href="admin.php?section=teachers_list">
                    <div class="panel panel-success">
						
							<div class="panel-heading" style="text-align: center; font-weight: bold;">Liste des ensegniants</div>
							<div class="panel-body">
								<img src="img/cours.jpg" style="width:100%;height: 200px;">
							</div>
							<div class="panel-footer"></div>
						
                    </div>
					</a>
                </div>
                <div class="col-xs-3">
				<a href="admin.php?section=students_list">
                    <div class="panel panel-warning">
						
							<div class="panel-heading" style="text-align: center; font-weight: bold;">Liste des etudiants</div>
							<div class="panel-body">
								<img src="img/td.jpg" style="width:100%;height: 200px;">
							</div>
							<div class="panel-footer"></div>
						
                    </div>
					</a>
                </div>
                <div class="col-xs-3">
					<a href="admin.php?section=administrators_list">
                    	<div class="panel panel-default">
						
							<div class="panel-heading" style="text-align: center; font-weight: bold;">Liste des administrateurs</div>
							<div class="panel-body">
								<img src="img/emploi.jpg" style="width:100%;height: 200px;">
							</div>
							<div class="panel-footer"></div>
						
                    	</div>
					</a>
                </div>';
						}
                    ?>
	            </div>
	        </div>
            <div class="row"><?php require_once"includes/footer.php"; ?></div>
        </div>
    </body>
</html>
