<?php require_once"includes/header.php"; ?>
        <title>Acueil</title>

    </head>
    <body>
        <div class="container">
            <div class="row">
                <?php require_once"includes/menu.php"; ?>
            </div>
            <div class="row" style="margin-top:40px;">
                <?php
                    if($connected){exit('<meta http-equiv="refresh" content="0; url=index.php">');}
                    if(empty($_POST["email"]) || empty($_POST["password_m"]) || empty($_POST["user_type"])){
                        echo "missing login informations!";
                       echo'<form action="login.php" method="post" name="login">
                       <div class="modal-body">
                       <div class="form-group">
                               <label for="user_type">Role</label>
                               <select name="user_type" id="user_type">
                                   <option value="student">Etudiant</option>
                                   <option value="teacher">Enseignant</option>
                                   <option value="admin">Administrateur</option>
                               </select>
                           </div>
                       <div class="form-group">
                               <label for="email">Email</label>
                               <input type="text" name="email"  class="form-control" id="email" aria-describedby="usernameHelp" placeholder="Email">
                           </div>
                           <div class="form-group">
                               <label for="password">Mot de pass</label>
                               <input type="password" name="password" id="password"  class="form-control" aria-describedby="emailHelp" placeholder="mot de pass">
                           </div>
                           
                       </div>
                       <div class="modal-footer">
                               <button type="submit" class="btn btn-sm btn-success">Login</button>
                       </div>
                   </form>'; 
                    }
                    else{
                        $sent_email = mysqli_real_escape_string($con, $_POST["email"]);
                        $sent_password = md5($_POST["password_m"]);
                        $sent_user_type = $_POST["user_type"];
                        if(!in_array($sent_user_type,["student","teacher","admin"])){
                            echo "unkown user ROLE";
                        }else{
                            if($sent_user_type == "student"){$bd_table = "etudiant";}
                            else if($sent_user_type == "teacher"){$bd_table = "enseignant";}
                            else if($sent_user_type == "admin"){$bd_table = "administrateur";}
                            $user_search_query = mysqli_query($con, "SELECT nom, prenom FROM $bd_table WHERE email='$sent_email' AND password='$sent_password'");
                            $count_users = mysqli_num_rows($user_search_query);
                            if($count_users != 1){
                                echo "Sorry!, we were unable to find this user.";
                            }else{
                                $_SESSION["email"] = $sent_email;
                                $_SESSION["user_type"] = $sent_user_type;
                                $user_row = mysqli_fetch_assoc($user_search_query);
                                $user_name = $user_row['nom'];
                                $user_firstname = $user_row['prenom'];
                                echo' <span class="alert alert success">Bienvenue <h6>'.$user_name." ".$user_firstname.'</h6> :)</span>';
                            }
                        }
                        echo'<meta http-equiv="refresh" content="3; url=index.php">';
                    }
                ?>
                
	        </div>
            <div class="row">
                
            </div>
            <div class="row"><?php require_once"includes/footer.php"; ?></div>
        </div>
    </body>
</html>
